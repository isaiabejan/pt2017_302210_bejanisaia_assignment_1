package polynomial.model;

public class Monom {
    private Double coef;
    private Integer degree;

    public Monom(Double coef, Integer degree) {
        this.coef = coef;
        this.degree = degree;
    }

    public Double getCoef() {

        return coef;
    }

    public void setCoef(Double coef) {

        this.coef = coef;
    }

    public Integer getDegree() {

        return degree;
    }

    public void setDegree(Integer degree) {

        this.degree = degree;
    }

    //creaza o copie a monomului curent
    public Monom getCopy() {
        return new Monom(this.coef, this.degree);
    }
}
