package polynomial.model;

public enum OperationType {
    ADD, SUBSTRACT, MULTIPLY, DIVISION, INTEGRATE, DERIVE
}
