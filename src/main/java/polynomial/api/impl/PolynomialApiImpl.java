package polynomial.api.impl;

import polynomial.api.PolynomialApi;
import polynomial.model.OperationType;
import polynomial.model.Polynomial;
import polynomial.service.RegexService;

public class PolynomialApiImpl implements PolynomialApi {
    private final RegexService regexService;

    public PolynomialApiImpl() {
        // initializez regex
        regexService = new RegexService();
    }

    // primeste request-ul de la UI si in functie de ce operatie este aleasa
    // aceasta o executa
    public Polynomial doOperation(String firstPolynomial, String secondPolynomial, OperationType type) {
        Polynomial polynomial1 = regexService.getPolynomial(firstPolynomial);
        Polynomial polynomial2 = regexService.getPolynomial(secondPolynomial);
        switch (type) {
            case ADD:
                return polynomial1.add(polynomial2);
            case SUBSTRACT:
                return polynomial1.substract(polynomial2);
            case MULTIPLY:
                return polynomial1.multiply(polynomial2);
            case DERIVE:
                return polynomial1.derive();
            case INTEGRATE:
                return polynomial1.integrate();
            case DIVISION:
                return polynomial1.divide(polynomial2);
        }
        return null;
    }
}
