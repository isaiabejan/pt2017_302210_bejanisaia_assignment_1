package polynomial.model;

import java.text.DecimalFormat;
import java.util.Comparator;
import java.util.Iterator;
import java.util.TreeSet;

public class Polynomial {
    private TreeSet<Monom> monoms = new TreeSet<Monom>(new DegreeComparator());

    // elimina ".0" daca un numar double nu are zecimale
    // iara daca are ia doar primele doua zecimale.
    public static String removeZero(Double number) {
        DecimalFormat format = new DecimalFormat("#.##");
        return format.format(number);
    }

    public TreeSet<Monom> getMonoms() {
        return monoms;
    }

    //adauga monom in TreeSet
    public void addMonom(Monom monom) {
        boolean duplicateDegree = false;
        if (monoms.isEmpty()) {
            monoms.add(monom);
        } else {
            for (Monom mMonom : monoms) {
                if (mMonom.getDegree().equals(monom.getDegree())) {
                    mMonom.setCoef(mMonom.getCoef() + monom.getCoef());
                    duplicateDegree = true;
                    break;
                }
            }

            if (!duplicateDegree) {
                monoms.add(monom);
            }
        }

        // remove the monoms that have the coef = 0
        Iterator<Monom> iter = monoms.iterator();
        while (iter.hasNext()) {
            Monom next = iter.next();
            if (next.getCoef() == 0) {
                iter.remove();
            }
        }
    }

    // metoda de adunare a doua polinoame
    public Polynomial add(Polynomial p) {
        Polynomial result = new Polynomial();

        for (Monom monom : this.getMonoms()) {
            result.addMonom(monom.getCopy());
        }

        for (Monom monom : p.getMonoms()) {
            result.addMonom(monom);
        }

        return result;
    }

    // inmultirea a 2 polinoame
    public Polynomial multiply(Polynomial p) {
        Polynomial result = new Polynomial();

        for (Monom m1 : this.getMonoms()) {
            for (Monom m2 : p.getMonoms()) {
                result.addMonom(new Monom(m1.getCoef() * m2.getCoef(), m1.getDegree() + m2.getDegree()));
            }
        }
        return result;
    }

    // scaderea a 2 polinoame
    public Polynomial substract(Polynomial p) {

        Polynomial minusP = new Polynomial();

        for (Monom monom : p.getMonoms()) {
            minusP.addMonom(new Monom(-monom.getCoef(), monom.getDegree()));
        }

        return minusP.add(this);
    }

    // impartirea unor polinoame dupa algoritmul matematic
    public Polynomial divide(Polynomial p) {
        Polynomial quotient = new Polynomial();

        Polynomial remainder = new Polynomial();
        Polynomial newP = new Polynomial();

        if(this.print().equals(p.print())) {
            quotient.addMonom(new Monom(1.0, 0));
        } else {
            for (Monom monom : this.getMonoms()) {
                newP.addMonom(monom.getCopy());
            }
            while (p.getMonoms().first().getDegree() <= newP.getMonoms().first().getDegree()) {
                quotient.addMonom(new Monom(newP.getMonoms().first().getCoef() / p.getMonoms().first().getCoef(), newP.getMonoms().first().getDegree() - p.getMonoms().first().getDegree()));
                Polynomial lastMonom = new Polynomial();
                lastMonom.addMonom(new Monom(quotient.getMonoms().last().getCoef(), quotient.getMonoms().last().getDegree()));
                remainder = p.multiply(lastMonom);
                remainder = newP.substract(remainder);
                newP.getMonoms().clear();
                for (Monom monom : remainder.getMonoms()) {
                    newP.addMonom(monom);
                }
                lastMonom.getMonoms().clear();
            }
        }

        return quotient;
    }

    // derivarea unui polinom
    public Polynomial derive() {

        Polynomial pDerived = new Polynomial();
        Polynomial result = new Polynomial();
        for (Monom monom : this.getMonoms()) {
            pDerived.addMonom(monom.getCopy());
        }

        for (Monom monom : pDerived.getMonoms()) {
            monom.setCoef(monom.getCoef() * monom.getDegree());
            monom.setDegree(monom.getDegree() - 1);
            result.addMonom(monom);
        }

        return result;
    }

    //integrarea unui polinom
    public Polynomial integrate() {

        Polynomial result = new Polynomial();
        for (Monom monom : this.getMonoms()) {
            result.addMonom(monom.getCopy());
        }

        for (Monom monom : result.getMonoms()) {
            monom.setDegree(monom.getDegree() + 1);
            monom.setCoef(monom.getCoef() / monom.getDegree());
        }
        return result;
    }

    // metoda de afisiare a unui polinom
    public String print() {
        String s = "";
        for(Monom monom : this.getMonoms()) {
            if (monom.getCoef() > 0.0) {
                if (monom.getDegree() == 0)
                    s += "+" + removeZero(monom.getCoef());
                else if (monom.getCoef() == 1.0)
                    if (monom.getDegree() != 1)
                        s += "+x^" + monom.getDegree();
                    else
                        s += "+x";
                else if (monom.getDegree() != 1)
                    s += "+" + removeZero(monom.getCoef()) + "x^" + monom.getDegree();
                else
                    s += "+" + removeZero(monom.getCoef()) + "x";
            }
            if (monom.getCoef() < 0.0)
                if (monom.getDegree() == 0)
                    s += removeZero(monom.getCoef());
                else if (monom.getCoef() != -1.0)
                    if (monom.getDegree() != 1)
                        s += removeZero(monom.getCoef()) + "x^" + monom.getDegree();
                    else
                        s += removeZero(monom.getCoef()) + "x";
                else if (monom.getDegree() != 1)
                    s += "-x^" + monom.getDegree();
                else
                    s += "-x";
        }
        // elimina caracteru "+" de pe prima pozitie (daca exista)
        if(s.indexOf("+") == 0) {
            s = s.substring(1,s.length());
        }
        return s;
    }

    class DegreeComparator implements Comparator<Monom> {
        // criteriu de ordonare a TreeSetului (descrescator)
        public int compare(Monom m1, Monom m2) {
            if (m1.getDegree() > m2.getDegree()) {
                return -1;
            } else {
                return 1;
            }
        }
    }
}

