package polynomial.service;

import polynomial.model.Monom;
import polynomial.model.Polynomial;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class RegexService {
    private final static String PATTERN = ("(-?(\\d+)?x(\\^-?\\d+)?|-?\\d+)");

    // transforma stringul input intr-un polinom dupa pattern-ul de mai sus
    public Polynomial getPolynomial(String input) {
        Pattern pattern = Pattern.compile(PATTERN);
        Matcher match = pattern.matcher(input);
        Polynomial polynomial = new Polynomial();
        Integer degree;
        Double coef;

        while (match.find()) {
            if (match.group().contains("x^")) {
                if (match.group().indexOf("x^") > 0) {
                    String[] parts = match.group().split(Pattern.quote("x^"));
                    if(parts[0] != "-")
                        coef = Double.parseDouble(parts[0]);
                    else
                        coef = -1.0;
                    degree = Integer.parseInt(parts[1]);
                } else {
                    coef = 1.0;
                    degree = Integer.parseInt(match.group().substring(match.group().indexOf("x^") + 2, match.group().length()));
                }

            } else if (match.group().contains("x")) {
                if (match.group().length() == 1) {
                    coef = 1.0;
                    degree = 1;
                } else {
                    if (match.group().equals("-x")) {
                        coef = -1.0;
                        degree = 1;
                    } else {
                        coef = Double.parseDouble(match.group().substring(0, match.group().indexOf("x")));
                        degree = 1;
                    }
                }
            } else {
                coef = Double.parseDouble(match.group());
                degree = 0;
            }
            polynomial.addMonom(new Monom(coef, degree));
        }
        return polynomial;
    }
}

