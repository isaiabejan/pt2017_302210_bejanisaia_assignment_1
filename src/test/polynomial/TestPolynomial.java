package polynomial;

import org.junit.Assert;
import org.junit.Test;
import polynomial.model.Monom;
import polynomial.model.Polynomial;

import java.util.Iterator;
import java.util.Set;


public class TestPolynomial {
    //testeaza adunarea a 2 polinoame:
    // p1 = x^1 + 1 si p2 = x^1 + 1
    @Test
    public void testAdd() {
        Polynomial polynomial1 = new Polynomial();
        polynomial1.addMonom(new Monom(1.0, 1));
        polynomial1.addMonom(new Monom(1.0, 0));

        Polynomial polynomial2 = new Polynomial();
        polynomial2.addMonom(new Monom(1.0, 1));
        polynomial2.addMonom(new Monom(1.0, 0));

        Polynomial result = polynomial1.add(polynomial2);

        Assert.assertEquals(2, result.getMonoms().size());
        Assert.assertEquals(Double.valueOf(2), result.getMonoms().first().getCoef());
        Assert.assertEquals(Integer.valueOf(1), result.getMonoms().first().getDegree());
        Assert.assertEquals(Double.valueOf(2), result.getMonoms().last().getCoef());
        Assert.assertEquals(Integer.valueOf(0), result.getMonoms().last().getDegree());
    }

    // testeaza scaderea a doua polinoame
    // p1 = 3x^2 + 2x + 5 si p2 = 2x^2 + 3x
    @Test
    public void testSubstract() {
        Polynomial polynomial1 = new Polynomial();
        polynomial1.addMonom(new Monom(3.0, 2));
        polynomial1.addMonom(new Monom(2.0, 1));
        polynomial1.addMonom(new Monom(5.0, 0));

        Polynomial polynomial2 = new Polynomial();
        polynomial2.addMonom(new Monom(2.0, 2));
        polynomial2.addMonom(new Monom(3.0, 1));

        Polynomial result = polynomial1.substract(polynomial2);

        Assert.assertEquals(3, result.getMonoms().size());

        Assert.assertEquals(Double.valueOf(1.0), result.getMonoms().first().getCoef());
        Assert.assertEquals(Integer.valueOf(2), result.getMonoms().first().getDegree());

        Monom monom2 = getByIndex(result.getMonoms(), 2);
        Assert.assertEquals(Double.valueOf(-1.0), monom2.getCoef());
        Assert.assertEquals(Integer.valueOf(1), monom2.getDegree());

        Assert.assertEquals(Double.valueOf(5.0), result.getMonoms().last().getCoef());
        Assert.assertEquals(Integer.valueOf(0), result.getMonoms().last().getDegree());
    }

    //testeaza inmultirea a 2 polinoame
    // p1 = 3x^2 + 2x si p2 = 2x^2 + 3x
    @Test
    public void testMultiply() {
        Polynomial polynomial1 = new Polynomial();
        polynomial1.addMonom(new Monom(3.0, 2));
        polynomial1.addMonom(new Monom(2.0, 1));

        Polynomial polynomial2 = new Polynomial();
        polynomial2.addMonom(new Monom(2.0, 2));
        polynomial2.addMonom(new Monom(3.0, 1));

        Polynomial result = polynomial1.multiply(polynomial2);

        Assert.assertEquals(3, result.getMonoms().size());

        Assert.assertEquals(Double.valueOf(6.0),result.getMonoms().first().getCoef());
        Assert.assertEquals(Integer.valueOf(4),result.getMonoms().first().getDegree());

        Monom monom2 = getByIndex(result.getMonoms(), 2);
        Assert.assertEquals(Double.valueOf(13.0),monom2.getCoef());
        Assert.assertEquals(Integer.valueOf(3),monom2.getDegree());

        Assert.assertEquals(Double.valueOf(6.0),result.getMonoms().last().getCoef());
        Assert.assertEquals(Integer.valueOf(2),result.getMonoms().last().getDegree());

    }

    // testeaza derivarea unui polinom
    // 4x^5 + 2x^2 + 6
    @Test
    public void testDerive() {
        Polynomial polynomial = new Polynomial();
        polynomial.addMonom(new Monom(4.0, 5));
        polynomial.addMonom(new Monom(2.0, 2));
        polynomial.addMonom(new Monom(6.0, 0));

        Polynomial result = polynomial.derive();

        Assert.assertEquals(2, result.getMonoms().size());

        Assert.assertEquals(Double.valueOf(20.0),result.getMonoms().first().getCoef());
        Assert.assertEquals(Integer.valueOf(4),result.getMonoms().first().getDegree());

        Assert.assertEquals(Double.valueOf(4.0),result.getMonoms().last().getCoef());
        Assert.assertEquals(Integer.valueOf(1),result.getMonoms().last().getDegree());
    }

    // testeaza integrarea unui polinom
    // 3x^5 + 4
    @Test
    public void testIntegrate() {
        Polynomial polynomial = new Polynomial();
        polynomial.addMonom(new Monom(3.0, 5));
        polynomial.addMonom(new Monom(4.0, 0));


        Polynomial result = polynomial.integrate();

        Assert.assertEquals(2, result.getMonoms().size());

        Assert.assertEquals(Double.valueOf(0.5),result.getMonoms().first().getCoef());
        Assert.assertEquals(Integer.valueOf(6),result.getMonoms().first().getDegree());

        Assert.assertEquals(Double.valueOf(4.0),result.getMonoms().last().getCoef());
        Assert.assertEquals(Integer.valueOf(1),result.getMonoms().last().getDegree());

    }

    // testea impartirea a 2 polinoame
    // p1 = 2x^3 + 3x^2 - x + 5 si p2 = x^2 - x +1
    @Test
    public void testDivide() {
        Polynomial polynomial1 = new Polynomial();
        polynomial1.addMonom(new Monom(2.0, 3));
        polynomial1.addMonom(new Monom(3.0, 2));
        polynomial1.addMonom(new Monom(-1.0, 1));
        polynomial1.addMonom(new Monom(5.0, 0));

        Polynomial polynomial2 = new Polynomial();
        polynomial2.addMonom(new Monom(1.0, 2));
        polynomial2.addMonom(new Monom(-1.0, 1));
        polynomial2.addMonom(new Monom(1.0, 0));

        Polynomial result = polynomial1.divide(polynomial2);

        Assert.assertEquals(2, result.getMonoms().size());

        Assert.assertEquals(Double.valueOf(2.0),result.getMonoms().first().getCoef());
        Assert.assertEquals(Integer.valueOf(1),result.getMonoms().first().getDegree());

        Assert.assertEquals(Double.valueOf(5.0),result.getMonoms().last().getCoef());
        Assert.assertEquals(Integer.valueOf(0),result.getMonoms().last().getDegree());
    }


    //accesare unui element anume
    private <T> T getByIndex(Set<T> collection, Integer index) {
        Iterator<T> it = collection.iterator();
        int i = 0;
        T current = null;
        while(it.hasNext() && i < index) {
            current = it.next();
            i++;
        }
        return current;
    }
}
