package polynomial.controller;

import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.TextField;
import polynomial.api.PolynomialApi;
import polynomial.api.impl.PolynomialApiImpl;
import polynomial.model.OperationType;
import polynomial.model.Polynomial;

import java.net.URL;
import java.util.ResourceBundle;

public class Controller implements Initializable {
    private PolynomialApi polynomialApi;

    @FXML
    private TextField firstPolynomial;
    @FXML
    private TextField secondPolynomial;
    @FXML
    private TextField result;

    public void initialize(URL location, ResourceBundle resources) {
        polynomialApi = new PolynomialApiImpl();
    }

    // cand butonul de adunare este apasat sa se execute adunare
    public void addButton() {
        String polynomial1 = firstPolynomial.getText();
        String polynomial2 = secondPolynomial.getText();
        Polynomial result = polynomialApi.doOperation(polynomial1, polynomial2, OperationType.ADD);
        this.result.setText(result.print());
    }

    // cand butonul de scadere este apasat sa se execute scadere
    public void substractButton() {
        String polynomial1 = firstPolynomial.getText();
        String polynomial2 = secondPolynomial.getText();
        Polynomial result = polynomialApi.doOperation(polynomial1, polynomial2, OperationType.SUBSTRACT);
        this.result.setText(result.print());
    }

    // cand butonnul de inmultire este apasat sa se execute inmultirea
    public void multiplyButton() {
        String polynomial1 = firstPolynomial.getText();
        String polynomial2 = secondPolynomial.getText();
        Polynomial result = polynomialApi.doOperation(polynomial1, polynomial2, OperationType.MULTIPLY);
        this.result.setText(result.print());
    }

    // cand butonul de derivare a primului polinom este apasat sa se execute derivarea
    public void deriveFirstPolynomialButton() {
        String polynomial = firstPolynomial.getText();
        Polynomial result = polynomialApi.doOperation(polynomial, polynomial, OperationType.DERIVE);
        this.result.setText(result.print());
    }

    // cand butonul de derivare a celui de-al doilea polinom este apasat sa se execute derivarea
    public void deriveSecondPolynomialButton() {
        String polynomial = secondPolynomial.getText();
        Polynomial result = polynomialApi.doOperation(polynomial, polynomial, OperationType.DERIVE);
        this.result.setText(result.print());
    }

    // cand butonul de integrare a primului polinom este apasat sa se execute integrarea
    public void integrateFirstPolynomialButton() {
        String polynomial = firstPolynomial.getText();
        Polynomial result = polynomialApi.doOperation(polynomial, polynomial, OperationType.INTEGRATE);
        this.result.setText(result.print());
    }

    // cand butonul de integrare a celui de-al doilea polinom este apasat sa se execute integrarea
    public void integrateSecondPolynomialButton() {
        String polynomial = secondPolynomial.getText();
        Polynomial result = polynomialApi.doOperation(polynomial, polynomial, OperationType.INTEGRATE);
        this.result.setText(result.print());
    }

    // cand butonul de impartire este apasat sa se execute impartirea
    public void divideButton() {
        String polynomial1 = firstPolynomial.getText();
        String polynomial2 = secondPolynomial.getText();
        Polynomial result = polynomialApi.doOperation(polynomial1, polynomial2, OperationType.DIVISION);
        this.result.setText(result.print());
    }
}