package polynomial.api;


import polynomial.model.OperationType;
import polynomial.model.Polynomial;

public interface PolynomialApi {
    Polynomial doOperation(String firstPolynomial, String secondPolynomial, OperationType type);
}
